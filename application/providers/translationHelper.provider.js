/*
* Translation helper to be used in the config process
* */
(function(){
    'use strict';

    /* @ngInject */
    translationHelperProvider.$inject = ['$translateProvider'];

    function translationHelperProvider($translateProvider) {

        return {
            configureTranslations : configureTranslations,
            $get : function(){}
        };

        function configureTranslations(language, translation){
            $translateProvider.translations(language, translation);
        }

    }

    angular
        .module('PortfolioAdmin.core')
        .provider('translationHelper', translationHelperProvider);
})();