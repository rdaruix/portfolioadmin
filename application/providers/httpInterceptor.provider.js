/*
* Interceptor service used to change request configs,
* treat response and responseErrors.
* */
(function(){
    // ecma6
    'use strict';

    // the service main function
    httpInterceptor.$inject = ['SessionService', '$rootScope', 'AppConfigs', '$location'];
    function httpInterceptor(SessionService, $rootScope, AppConfigs, $location){
        // exports public methods

        return {
            request : request,
            response: response,
            responseError : responseError
        };

        // intercept all requests
        function request(config){
            // gets the current user cookie object
            var currentUser = SessionService.get('jui');
            // check if the cookie is alive
            if(currentUser){
                // we only get the application/json types and ignore the rest
                if(config.url.indexOf(AppConfigs.apiEndpoint) !== -1){
                    // injects the authorization token
                    config.headers['Authorization'] = currentUser.authToken;
                }
            }
            // return the configs so the request can proceed
            return config;
        }

        function response(response) {
            if (response.data && response.data.error && response.data.error.match(/auth|token/i)) {
                $rootScope.$broadcast('loginRequired', {rejection:response});
            }else{
                SessionService.set('dash', {showDashboard:true});
            }

            // return the response
            return response;
        }

        function responseError(rejection){

            switch (rejection.status) {
                case 401:
                    $rootScope.$broadcast('loginRequired', {rejection:rejection});
                    return rejection;
                    break;
            }

            // retorna o reject para a resposta, isso bloqueia requisições
            // e dispara as ações corretas nos outros interceptadores
            return rejection;
        }

    }

    angular
        .module('PortfolioAdmin.core')
        .factory('httpInterceptor', httpInterceptor);

})();