/*
 * The main routes for the app
 * */
(function(){
    // ecma6 / jshint
    'use strict';

    /*
     * Get the returned states from getStates function
     * and send it to the configureStates function that
     * do the registry of new states/routes
     * */
    function appRun(routerHelper) {
        // calls the state registration method
        routerHelper.configureStates(getStates());
    }

    /*
     * Return the list of objects containing the states
     * to register
     * */
    function getStates() {
        return [
            {
                state: 'admin',
                config: {
                    abstract : true,
                    views: {
                        'admin': {templateUrl : 'application/layouts/admin.layout.html'}
                    }
                }
            },
            {
                state : 'login',
                config: {
                    abstract : true,
                    views: {
                        'login': {templateUrl : 'application/layouts/login.layout.html'}
                    },
                    data: {
                        bodyClass: 'signin'
                    }
                }
            },
            {
                state : 'signup',
                config: {
                    abstract : true,
                    views: {
                        'signup': {templateUrl : 'application/layouts/signup.layout.html'}
                    },
                    data: {
                        bodyClass: 'signin'
                    }
                }
            },
            {
                state : 'profile',
                config: {
                    abstract : true,
                    views: {
                        'profile': {templateUrl : 'application/layouts/profile.layout.html'}
                    },
                    data: {
                        bodyClass: 'signup'
                    }
                }
            }
        ];
    }

    // run the config code inside the app main module
    angular
        .module('PortfolioAdmin.core')
        .run(appRun);

})();