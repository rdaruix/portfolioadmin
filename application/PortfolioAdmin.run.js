/*
* App .run block, everything here run after the .config block
* **/
(function(){
    'use strict';

    PortfolioAdminRun.$inject = ['CheckAuthSession', 'ToastrTemplate'];
    function PortfolioAdminRun(CheckAuthSession, ToastrTemplate){

        /**
         * Auth session checker initialized
         * */
        CheckAuthSession.initialize();

        /**
         * Toastr Module Template change
         * */
        ToastrTemplate.initialize();
    }

    angular
        .module('PortfolioAdmin.core')
        .run(PortfolioAdminRun);

})();