(function(){
    'use strict';


    PortfolioDashboardCtrl.$inject = ['$scope', 'PortfolioDashboardService', '$stateParams'];
    function PortfolioDashboardCtrl($scope, PortfolioDashboardService,$stateParams){

        var dashboardCtrl = this,
            caseItem = {},
            formattedTechs = null;

        init();

        function init() {

            getLastCase();
        }

        function getLastCase(){
            PortfolioDashboardService.getLastCase()
                .then(function(result){
                    if(result.slug === 'response-ok'){

                        for(var i = 0; i < result.data.technologies.length; i++){
                            var technology = result.data.technologies[i];
                            if(!dashboardCtrl.formattedTechs){
                                dashboardCtrl.formattedTechs = technology.name;
                            }else{
                                dashboardCtrl.formattedTechs = dashboardCtrl.formattedTechs + ", " + technology.name;
                            }
                        };

                        dashboardCtrl.caseItem = result.data;

                    }
                })
        }


        // exports
        angular.extend(this, {
            /* vars */
            dashboardCtrl:dashboardCtrl,
            formattedTechs: formattedTechs,
            caseItem: caseItem
        });
    }

    angular
        .module('PortfolioAdmin.portfolio.dashboard')
        .controller('PortfolioDashboardCtrl', PortfolioDashboardCtrl)

})();