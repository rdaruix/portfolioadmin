(function(){
    'use strict';

    PortfolioDashboardService.$inject = ['$http', 'AppConfigs'];
    function PortfolioDashboardService($http, AppConfigs){

        function getLastCase(){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/job/lastJob/')
                .then(getLastCaseSuccess)
                .catch(getLastCaseFail);

            function getLastCaseSuccess(response){
                return response.data;
            }
            function getLastCaseFail(error){
                return error.data;
            }
        }

        // exports
        return {

            getLastCase: getLastCase
        };
    }

    angular
        .module('PortfolioAdmin.portfolio.dashboard')
        .factory('PortfolioDashboardService', PortfolioDashboardService);

})();