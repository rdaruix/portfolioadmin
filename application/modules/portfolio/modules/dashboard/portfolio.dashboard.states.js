/*
 * The routes for the dashboard module from the maker module
 * */
(function(){
    // ecma6 / jshint
    'use strict';

    /*
     * Get the returned states from getStates function
     * and send it to the configureStates function that
     * do the registry of new states/routes
     * */
    function appRun(routerHelper) {
        // calls the state registration method
        routerHelper.configureStates(getStates());
    }

    /*
     * Return the list of objects containing the states
     * to register
     * */
    function getStates() {
        return [
            {
                state : 'admin.portfolio.dashboard',
                config : {
                    url : '/dashboard',
                    templateUrl : 'application/modules/portfolio/modules/dashboard/partials/portfolio.dashboard.partial.html',
                    data : {
                        titlesTop: {
                            title : "Dashboard"
                        }
                    }
                }
            }
        ];
    }

    // run the config code inside the app main module
    angular
        .module('PortfolioAdmin.portfolio.dashboard')
        .run(appRun);

})();
