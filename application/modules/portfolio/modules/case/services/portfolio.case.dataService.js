(function(){
    'use strict';

    PortfolioCaseAddEditService.$inject = ['$http', 'AppConfigs'];
    function PortfolioCaseAddEditService($http, AppConfigs){

        var model = {
            name:null,
            description:null,
            date_job:null,
            image:null,
            technologies: []
        };

        function getCategories(){

            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/category/categories')
                .then(getCategoriesSuccess)
                .catch(getCategoriesFail);

            function getCategoriesSuccess(response){
                return response.data;
            }
            function getCategoriesFail(error){
                return error.data;
            }
        }

        function getTechnologies(){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/technology/technologies')
                .then(getTechnologiesSuccess)
                .catch(getTechnologiesFail);

            function getTechnologiesSuccess(response){
                return response.data;
            }
            function getTechnologiesFail(error){
                return error.data;
            }
        }

        function saveCase(data){
            var apiUrl = AppConfigs.apiEndpoint;


            var formData = new FormData();
            angular.forEach(data, function(value, index){
                // when the index is for the image object
                // things needs to change a bit
                if(index == 'image' && value != null){
                    formData.append('image', value);
                } else if(index == 'technologies'){
                    formData.append('technologies', JSON.stringify(value))
                }else {
                    // for everything else, just appends to the formData
                    formData.append(index, value);
                }

            });

            return $http.post(apiUrl+'/job/addJob', formData, {
                headers: {'Content-Type': undefined}
            })
                .then(saveCaseSuccess)
                .catch(saveCaseFail);

            function saveCaseSuccess(response){
                return response.data;
            }
            function saveCaseFail(error){
                return error.data;
            }

        }

        function getCase(caseId){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/job/findJob/' + caseId)
                .then(getCaseSuccess)
                .catch(getCaseFail);

            function getCaseSuccess(response){
                return response.data;
            }
            function getCaseFail(error){
                return error.data;
            }
        }

        function getAllCases(){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/job/jobs/')
                .then(getAllCasesSuccess)
                .catch(getAllCasesFail);

            function getAllCasesSuccess(response){
                return response.data;
            }
            function getAllCasesFail(error){
                return error.data;
            }

        }

        function removeCase(caseId){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/job/deleteJob/' + caseId)
                .then(removeCaseSuccess)
                .catch(removeCaseFail);

            function removeCaseSuccess(response){
                return response.data;
            }
            function removeCaseFail(error){
                return error.data;
            }
        }

        // exports
        return {
            /* vars */
            model:model,
            /* methods */
            getCategories : getCategories,
            getTechnologies : getTechnologies,
            saveCase : saveCase,
            getCase: getCase,
            getAllCases: getAllCases,
            removeCase: removeCase
        };
    }

    angular
        .module('PortfolioAdmin.portfolio.case')
        .factory('PortfolioCaseAddEditService', PortfolioCaseAddEditService);

})();