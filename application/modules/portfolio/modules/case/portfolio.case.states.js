(function() {
    // ecma6 / jshint
    'use strict';

    /*
     * Get the returned states from getStates function
     * and send it to the configureStates function that
     * do the registry of new states/routes
     * */
    function appRun(routerHelper, $state) {
        // calls the state registration method
        routerHelper.configureStates(getStates($state));
    }

    /*
     * Return the list of objects containing the states
     * to register
     * */
    function getStates($state) {
        return [
            {
                state : 'admin.portfolio.case-list',
                config : {
                    url : '/case',
                    templateUrl : 'application/modules/portfolio/modules/case/partials/portfolio.case.list.partial.html',
                    data : {
                        contentClass: "case-list",
                        titlesTop: {
                            title : "Lista de Cases"
                        }
                    }
                }
            },
            {
                state : 'admin.portfolio.add-case',
                config : {
                    url : '/case/add',
                    templateUrl : 'application/modules/portfolio/modules/case/partials/portfolio.case.add-edit.partial.html',
                    data : {
                        contentClass: "add-edit-case",
                        titlesTop: {
                            title : "Cadastro de Case"
                        }
                    }
                }
            },
            {
                state : 'admin.portfolio.edit-case',
                config : {
                    url : '/case/edit/{caseId:int}',
                    templateUrl : 'application/modules/portfolio/modules/case/partials/portfolio.case.add-edit.partial.html',
                    data : {
                        contentClass: "add-edit-case",
                        titlesTop: {
                            title : "Editar Case"
                        }
                    }
                }
            }
        ]
    }

    // run the config code inside the app main module
    angular
        .module('PortfolioAdmin.portfolio.case')
        .run(appRun);
})();