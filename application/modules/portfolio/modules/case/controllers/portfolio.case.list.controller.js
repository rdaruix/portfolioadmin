
(function () {

    'use strict';

    PortfolioCaseListCtrl.$inject = ['$scope', 'PortfolioCaseAddEditService', '$stateParams'];
    function PortfolioCaseListCtrl($scope, PortfolioCaseAddEditService, $stateParams) {

        var caseCtrl = this,
            modelList = [];

        init();

        function init() {

            getCaseList();
        }

        function getCaseList(){
            PortfolioCaseAddEditService.getAllCases()
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        caseCtrl.modelList = result.data;
                    }
                })
        }

        function removeCase(id){
            PortfolioCaseAddEditService.removeCase(id)
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        swal(
                            'Sucesso!',
                            'Case salvo com sucesso',
                            'success'
                        );
                        getCaseList();
                    }
                });

        }

        // exports
        angular.extend(this, {
            /* vars */
            modelList: modelList,

            removeCase: removeCase
        });

    }


    angular
        .module('PortfolioAdmin.portfolio.case')
        .controller('PortfolioCaseListCtrl', PortfolioCaseListCtrl)

})();