
(function () {

    'use strict';

    PortfolioCaseAddEditCtrl.$inject = ['$scope', 'PortfolioCaseAddEditService', '$stateParams', '$mdDialog'];
    function PortfolioCaseAddEditCtrl($scope, PortfolioCaseAddEditService, $stateParams, $mdDialog) {

        var caseCtrl = this,
            model = angular.copy(PortfolioCaseAddEditService.model),
            caseId = $stateParams.caseId;
            caseCtrl.modelRead = {
                categories: [],
                technologies: []
            };

        init();

        function init() {

            if(!caseId){
                getCategories();
                getTechnologies();
            }else{
                getCategories();
                getTechnologies();
                getCase(caseId);
            }
        }

        function getCategories(){
            PortfolioCaseAddEditService.getCategories()
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        caseCtrl.modelRead.categories = result.data;
                    }
                })
        }

        function getTechnologies(){
            PortfolioCaseAddEditService.getTechnologies()
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        caseCtrl.modelRead.technologies = result.data;
                    }
                })
        }

        function getCase(caseId){
            PortfolioCaseAddEditService.getCase(caseId)
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        caseCtrl.model = result.data;
                    }
            })
        }

        function save(){
            PortfolioCaseAddEditService.saveCase(caseCtrl.model)
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        swal(
                            'Sucesso!',
                            'Case salvo com sucesso',
                            'success'
                        );
                        caseCtrl.model = angular.copy(PortfolioCaseAddEditService.model);
                    }
                })
        }

        function newTechnology(){

            $mdDialog.show({
                controller: CaseModeNewTechnologyCtrl,
                controllerAs: "dialogNewTechnologyMode",
                templateUrl: "new-technology.modal.tpl.html",
                clickOutsideToClose: true,
                escapeToClose : false
            });
        }


        CaseModeNewTechnologyCtrl.$inject = ["PortfolioTechnologyAddEditService"];
        function CaseModeNewTechnologyCtrl(PortfolioTechnologyAddEditService){

            var technologyCtrl = this,
                model = angular.copy(PortfolioTechnologyAddEditService.model);

            function save(){
                PortfolioTechnologyAddEditService.saveTechnology(technologyCtrl.model)
                    .then(function(result){
                        if(result.slug === 'response-ok'){
                            swal(
                                'Sucesso!',
                                'Tecnologia salva com sucesso',
                                'success'
                            );
                            $mdDialog.cancel();
                            getTechnologies();
                        }
                    })
            }

            function cancel(){
                $mdDialog.cancel();
            }

            angular.extend(this, {
                technologyCtrl:technologyCtrl,
                model:model,
                cancel: cancel,
                save:save
            });

        }

        // exports
        angular.extend(this, {
            /* vars */
            model: model,

            save:save,
            newTechnology:newTechnology
        });

    }


    angular
        .module('PortfolioAdmin.portfolio.case')
        .controller('PortfolioCaseAddEditCtrl', PortfolioCaseAddEditCtrl)

})();