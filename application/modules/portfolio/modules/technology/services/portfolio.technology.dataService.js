(function(){
    'use strict';

    PortfolioTechnologyAddEditService.$inject = ['$http', 'AppConfigs'];
    function PortfolioTechnologyAddEditService($http, AppConfigs){

        var model = {
            name: null
        };

       function saveTechnology(data){
           var apiUrl = AppConfigs.apiEndpoint;

           return $http.post(apiUrl+'/technology/addTechnology', data)
               .then(saveTechnologySuccess)
               .catch(saveTechnologyFail);

           function saveTechnologySuccess(response){
               return response.data;
           }

           function saveTechnologyFail(error){
               return error.data
           }
       }

        // exports
        return {
            /* vars */
            model:model,
            /* methods */
            saveTechnology : saveTechnology
        };
    }

    angular
        .module('PortfolioAdmin.portfolio.technology')
        .factory('PortfolioTechnologyAddEditService', PortfolioTechnologyAddEditService);

})();