(function() {
    // ecma6 / jshint
    'use strict';

    /*
     * Get the returned states from getStates function
     * and send it to the configureStates function that
     * do the registry of new states/routes
     * */
    function appRun(routerHelper, $state) {
        // calls the state registration method
        routerHelper.configureStates(getStates($state));
    }

    /*
     * Return the list of objects containing the states
     * to register
     * */
    function getStates($state) {
        return [
            {
                state : 'admin.portfolio.technology-add',
                config : {
                    url : '/technology',
                    templateUrl : 'application/modules/portfolio/modules/technology/partials/portfolio.technology.add.partial.html',
                    data : {
                        contentClass: "technology",
                        titlesTop: {
                            title : "Cadastro de Tecnologia"
                        }
                    }
                }
            }

        ]
    }

    // run the config code inside the app main module
    angular
        .module('PortfolioAdmin.portfolio.technology')
        .run(appRun);
})();