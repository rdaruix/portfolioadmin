(function(){
    'use strict';

    angular.module('PortfolioAdmin.portfolio', [
        /* shared modules */
        'PortfolioAdmin.core',
        'PortfolioAdmin.portfolio.dashboard',
        'PortfolioAdmin.portfolio.case',
        'PortfolioAdmin.portfolio.technology'
    ]);

})();