/*
 * Core module
 * Contains third part modules, ng modules and shared modules
 * that are used in all parts of this application
 *
 * Reference: https://github.com/johnpapa/angular-styleguide#modularity
 * */
(function(){
    'use strict';

    angular.module('PortfolioAdmin.core', [
        /* Angular modules */
        'ngMessages',
        'ngCookies',
        'ngSanitize',
        'ngMaterial',
        'ngAnimate',
        'ngAria',
        /* 3rd-party modules */
        'ui.router',
        'ui.bootstrap',
        'checklist-model',
        'toastr',
        'file-model',
        'pascalprecht.translate',
        '720kb.datepicker'
    ]);

})();