/*
 * The main routes for the app
 * */
(function(){
    // ecma6 / jshint
    'use strict';

    /*
     * Get the returned states from getStates function
     * and send it to the configureStates function that
     * do the registry of new states/routes
     * */
    function appRun(routerHelper) {
        // calls the state registration method
        routerHelper.configureStates(getStates());
    }

    /*
     * Return the list of objects containing the states
     * to register
     * */
    function getStates() {
        return [
            {
                state : 'login.auth',
                config : {
                    url : '/login',
                    templateUrl : 'application/modules/admin/login/partials/login.auth.partial.html',
                    data : {
                        bodyClass : 'loginDaruix'
                    }
                }
            }
        ];
    }

    // run the config code inside the app main module
    angular
        .module('PortfolioAdmin.login')
        .run(appRun);

})();