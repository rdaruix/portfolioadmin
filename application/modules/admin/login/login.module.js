/**
 * Login feature module, this module injects
 * shared modules and particular modules for this feature
 * */
(function(){
    'use strict';
    /* the core module is loaded here to
     make the login module plug-and-play */
    angular.module('PortfolioAdmin.login', [
        'PortfolioAdmin.core'
    ]);

})();