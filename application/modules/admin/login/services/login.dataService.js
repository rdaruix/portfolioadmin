(function(){
    'use strict';

    LoginDataService.$inject = ['$http', 'AppConfigs', 'SessionService'];
    function LoginDataService($http, AppConfigs, SessionService){

        // exports the public methods
        return {
            doAuth : doAuth,
            doLogout : doLogout,
            doRecovery : doRecovery,
            doPasswordChange : doPasswordChange,
            refreshAuthToken : refreshAuthToken,
            checkRecoveryToken : checkRecoveryToken
        };

        ////////////////////

        /*
        * Do the POST to the API login route
        * @param {object} data
        * data :
        * {
        *   email : string,
        *   password : string
        * }
        * */
        function doAuth(data){

            var apiUrl = AppConfigs.apiEndpoint;

            return $http.post(apiUrl+'/user/public/login', data)
                .then(doAuthComplete)
                .catch(doAuthFailed);

            function doAuthComplete(response){

                // updates the cookie information
                // 'jui' stands for: Joox User Info
                if(response.data.slug !== 'invalid-email-password' && response.data.slug !== 'without-user'){

                    var cookieData = {
                        authToken : response.data.token,
                        userLogged : response.data.userLogged
                    };

                    SessionService.set('jui', cookieData);
                    SessionService.set('dash', {showDashboard:true});
                }else if(response.data.slug === 'without-user'){

                    SessionService.set('userNotFound', 'Usuário não encontrado');
                }


                return response.data;
            }

            function doAuthFailed(error){
                return error.data;
            }
        }

        /*
        * Do the POST to the API logout route
        * */
        function doLogout(){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.post(apiUrl+'/account/logout')
                .then(doLogoutComplete)
                .catch(doLogoutFailed);

            function doLogoutComplete(response){
                return response.data;
            }

            function doLogoutFailed(error){
                return error.data;
            }
        }

        /*
         * Do the POST to the password recovery route
         * $param {object} data
         * data :
         * {
         *   email : string
         * }
         * */
        function doRecovery(data){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.post(apiUrl+'/account/password-reset-token', data)
                .then(doRecoveryComplete)
                .catch(doRecoveryFailed);

            function doRecoveryComplete(response){
                return response.data;
            }

            function doRecoveryFailed(error){
                return error.data;
            }
        }

        /*
        * Send a POST request to check if the token
        * to recovery the password is valid
        * data :
        * {
        *   token : string
        * }
        * */
        function checkRecoveryToken(data){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.post(apiUrl+'/account/password-verify-reset-token', data)
                .then(checkRecoveryTokenComplete)
                .catch(checkRecoveryTokenFailed);

            function checkRecoveryTokenComplete(response){
                return response.data;
            }

            function checkRecoveryTokenFailed(error){
                return error.data;
            }

        }

        /**
         * Refreshes the current auth token to update its information
         * @returns {Promise}
         */
        function refreshAuthToken(){

            var apiUrl = AppConfigs.apiEndpoint;

            return $http.post(apiUrl+'/account/login/refresh-token')
                .then(refreshAuthTokenComplete)
                .catch(refreshAuthTokenFailed);

            function refreshAuthTokenComplete(response){

                if(response.data.slug === "refresh-token-ok"){
                    SessionService.set('jui', {
                        authToken : response.data.data.authToken,
                        roles : response.data.data.roles.data
                    });
                }

                return response.data;
            }

            function refreshAuthTokenFailed(error){
                return error.data;
            }
        }

        /*
        * Send a PUT request to change the password
        * with the recovery token
        * data :
        * {
        *   token : string
        *   password : string
        * }
        * */
        function doPasswordChange(data){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.put(apiUrl+'/account/password-change-token', data)
                .then(passwordChangeComplete)
                .catch(passwordChangeFailed);

            function passwordChangeComplete(response){
                return response.data;
            }

            function passwordChangeFailed(error){
                return error.data;
            }
        }


    }

    angular
        .module('PortfolioAdmin.login')
        .factory('LoginDataService', LoginDataService);

})();