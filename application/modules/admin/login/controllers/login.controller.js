/*
* Login page controller, contains mainly the methods needed to
* validate and execute the auth
* */
(function(){
    'use strict';

    // the controller function and DI
    LoginCtrl.$inject = ['LoginDataService', '$scope', '$rootScope', '$state', 'SessionService', '$cookies', '_'];
    function LoginCtrl(LoginDataService, $scope, $rootScope, $state, SessionService, $cookies, _){

        // model for login form
        var auth = {
            user : {
                email: null,
                password: null
            },
            alert : {
                type : 'danger',
                timeout : 6000,
                show : false
            },
            result : {},
            submitDisabled : false,
            notFound: null
        };

        // model for password recover form
        var recovery = {
            user : {
                email : null
            },
            alert : {
                type : 'danger',
                timeout : 6000,
                show : false
            },
            result : {},
            submitDisabled : false
        };

        // model for password change form
        var passwordChange = {
            user : {
                newPassword : null,
                confirmNewPassword : null
            },
            alert : {
                type : 'danger',
                show : false
            },
            result : {},
            hideForm : true,
            submitDisabled : false,
            token : null
        };

        // gets the main object so we can
        // update things inside the main $scope
        // after some service is called
        var self = this;

        /* if there is a jui cookie, fill the information for the login form */
        var account = SessionService.get('account');
        if(account && !account.error && account.error !== "Unauthorized."){
            // the user email
            auth.user.email = null;
        }

        // method to execute the authentication
        function doAuth(loginForm){
            if(loginForm && (loginForm.$invalid || self.auth.submitDisabled))
                return false;

            LoginDataService.doAuth(this.auth.user)
                .then(function(result){

                    if(result.slug === 'response-ok'){
                        $state.go('admin.portfolio.dashboard');
                        this.auth.submitDisabled=true;
                        return true;
                    }else{
                        var accountNotFount = SessionService.get('userNotFound');
                        if(accountNotFount){
                            auth.notFound = accountNotFount;
                            this.auth.submitDisabled=false;
                        }
                    }
                });

            // disables the submit button

        }
        /*
        * $scope listeners methods
        * */

        // listens the stateChangeSuccess event to check if the token is valid
        $scope.$on('$stateChangeSuccess', checkRecoveryToken);

        /*
        * Generic private function used to change or add
        * information in the view-model objects
        * */
        function sendResultToModel(event, model, results){
            angular.forEach(results, function(value, key){
                self[model][key] = value;
            });
        }

        /* $stateChangeSuccess event */
        function checkRecoveryToken(event, toState, toParams){
            // checks for the route and param presents
            if(toState.name=="login.recoveryConfirm"){
                // looks for
                if(toParams.token){
                    LoginDataService.checkRecoveryToken(toParams)
                        .then(function(result){
                            // validates the return slug
                            if(result.slug === 'token-valid'){

                                // if valid, sets the info the password, change the vm
                                self.passwordChange.result = result;
                                self.passwordChange.token = toParams.token

                            } else if(
                                result.slug === 'invalid-expired-token' ||
                                result.slug === 'invalid-expired-token-account' ||
                                result.slug === 'invalid-token'
                            ){
                                $state.go('login.recovery').then(function(){
                                    result.message = $translate.instante('LOGIN_RECOVERY_TITLE_LINK');
                                    var resultObject = {
                                        result : result,
                                        alert : {
                                            show : true,
                                            type : 'danger'
                                        }
                                    };
                                    $rootScope.$broadcast('passwordRecovery:invalid-token', 'recovery', resultObject);
                                });
                            }
                        });
                } else {
                    $state.go('login.recovery').then(function(){
                        result.message = $translate.instante('LOGIN_RECOVERY_TITLE_LINK');
                        $rootScope.$broadcast('passwordRecovery:invalid-token', result);
                    });
                }
            }
        }

        // exports public methods/vars
        angular.extend(this, {
            /* vars */
            auth : auth,
            recovery : recovery,
            passwordChange : passwordChange,
            /* functions */
            doAuth : doAuth,
            checkRecoveryToken:checkRecoveryToken,
            sendResultToModel:sendResultToModel
        });
    }

    angular
        .module('PortfolioAdmin.login')
        .controller('LoginCtrl',LoginCtrl);

})();