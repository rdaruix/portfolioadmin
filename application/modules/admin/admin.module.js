(function(){
    'use strict';

    angular.module('PortfolioAdmin.admin', [
        /* shared module */
        'PortfolioAdmin.core',
        'PortfolioAdmin.structure',
        'PortfolioAdmin.account',
        'PortfolioAdmin.login'
/*
        /!* admin general modules *!/
        '
        'MakerAdmin.company',*/
        /*'MakerAdmin.login',
        'MakerAdmin.profile',
        'MakerAdmin.signup',
        'MakerAdmin.users'*/

    ]);

})();