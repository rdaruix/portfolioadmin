(function(){

    'use strict';

    function adminNavbarTop(){

        adminNavbarTopCtrl.$inject = [
            'AppConfigs', '$window', '$state', '$cookies', '$scope', '$location', 'AccountDataService', 'SessionService',
            'StructureService', 'LoginDataService', 'RecentActivitySocket', 'toastr'
        ];
        function adminNavbarTopCtrl(
            AppConfigs, $window, $state, $cookies, $scope, $location, AccountDataService, SessionService, StructureService, LoginDataService,
            RecentActivitySocket, toastr
        ){


            // show/hide the dropdown menu status var
            var dropDownOpen = false;

            // function to redirect the user
            // to whenever he wants to go, but first
            // close the dropdown menu
            function goTo(state, params){
                this.dropDownOpen = false;
                $state.go(state, params);
            }

            /*
            * Do the logout by removing the cookies, localStorage
            * and redirecting the user to the login page
            * */
            function doLogout(){

                SessionService.remove('jui'); // joox user info
                SessionService.remove('account'); // account info
                SessionService.remove('company'); // company info
                $location.url('/');

            }
            /* exports the public methods and vars to the view */
            angular.extend(this, {
                /*vars*/
                dropDownOpen: dropDownOpen,
                /*methods*/
                goTo : goTo,
                doLogout : doLogout
            });

        }

        return {
            strict : 'E',
            controller : adminNavbarTopCtrl,
            controllerAs : 'vmNavbarTop',
            templateUrl : 'application/modules/admin/structure/directives/navbar-top/navbar-top.partial.html'
        }
    }

    angular
        .module('PortfolioAdmin.structure')
        .directive('adminNavbarTop', adminNavbarTop)


})();