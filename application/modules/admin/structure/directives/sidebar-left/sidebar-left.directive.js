/*
 * Sidebar left directive made
 * for dashboard page, which includes:
 * - Company name
 * - Active profile selection
 * - Left menu links
 * */
(function () {
    'use strict';
    function adminSidebarLeft() {

        adminSidebarLeftCtrl.$inject = ['StructureService', '$state', '$scope', '$window'];
        function adminSidebarLeftCtrl(StructureService, $state, $scope, $window) {

            // When the state changes, sets the active menu item
            $scope.$on('$stateChangeStart', function (e, toState) {
                angular.forEach(StructureService.leftSideBar.menuActive, function (item) {
                    item.active = item.state === toState.name;
                });
            });

            // when the directive is loaded, sets the active menu item
            angular.forEach(StructureService.leftSideBar.menuItems, function (items) {
                angular.forEach(items, function(item){

                    item.active = item.state === $state.$current.name;
                })
            });

            //angular.element(document).find('aside').css('height', window.innerHeight)
        }

        return {
            strict: 'E',
            controller: adminSidebarLeftCtrl,
            controllerAs: 'leftSidebar',
            templateUrl: 'application/modules/admin/structure/directives/sidebar-left/sidebar-left.partial.html'
        }
    }

    angular
        .module('PortfolioAdmin.structure')
        .directive('adminSidebarLeft', adminSidebarLeft)


})();