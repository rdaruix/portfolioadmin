/*
* Directive to control and change the top title
* and sub title for each page
* */
(function(){

    'use strict';

    function adminTitlesTop(){

        function adminTitlesTopCtrl(){
            // nothing here for now
        }

        return {
            strict : 'E',
            controller : adminTitlesTopCtrl,
            controllerAs : 'vm',
            templateUrl : 'application/modules/admin/structure/directives/titles-top/titles-top.partial.html'
        }
    }

    angular
        .module('PortfolioAdmin.structure')
        .directive('adminTitlesTop', adminTitlesTop)
})();