/*
 * Admin structure main controller
 * */
(function () {

    'use strict';

    StructureCtrl.$inject = ['$scope', '$state', '$translate', 'AccountDataService', 'StructureService', 'RecentActivitySocket', 'SessionService'];
    function StructureCtrl($scope, $state, $translate, AccountDataService, StructureService, RecentActivitySocket, SessionService) {

        var vm = this,
            titlesTop = {},
            account = AccountDataService.currentAccount,
            company = AccountDataService.currentCompany,
            leftSideBar = StructureService.leftSideBar;

        if($state.$current.data){
            // and also checks if there is a titlesTop node in the data object
            titlesTop = ($state.$current.data.titlesTop) ? $state.$current.data.titlesTop : {};
        }

        if(!StructureService.fromLogin){
            AccountDataService.getAccount()
                .then(function(result){

                    var currentAccount = SessionService.get('account');
                    var accountRole = null;

                    angular.forEach(currentAccount.roles, function(role){
                        accountRole = role;
                    });

                    leftSideBar.menuActive = leftSideBar.menuItems[accountRole];
                    return result;
                });
        }

        /**
         * Scope event handlers
         */

        // when the state start to change, gets the title information in it and passes to the vm
        $scope.$on('$stateChangeStart', function (e, toState) {
            vm.titlesTop = {}; // resets the var
            if (toState.data) {
                // set it again
                vm.titlesTop = (toState.data.titlesTop) ? toState.data.titlesTop : {};
            }
        });

        $scope.$on('currentAccount:available', function(e, accData){
            vm.account = accData;
        });
        // check if there is something in the data node from the current state
        if ($state.$current.data) {
            // and also checks if there is a titlesTop node in the data object
            titlesTop = ($state.$current.data.titlesTop) ? $state.$current.data.titlesTop : {};
        }
        // exports
        angular.extend(this, {
            /* vars */
            account: account,
            company: company,
            titlesTop: titlesTop,
            leftSideBar: leftSideBar
        });

    }


    angular
        .module('PortfolioAdmin.structure')
        .controller('StructureCtrl', StructureCtrl)

})();