(function(){
    'use strict';

    /*
    * Structure general service
    * */

    StructureService.$inject = ['AccountDataService', 'SessionService', '_', '$q', '$state'];
    function StructureService(AccountDataService, SessionService, _, $q, $state){

        var fromLogin = false;

        var self = this;
        /*
        * The model for the left side bar directive
        * */
        var leftSideBar = {
            showSideBar : true,
            menuActive : {},
            menuItems : {
                'admin' : [
                    {label : 'Dashboard', state : 'admin.portfolio.dashboard', iconClass : 'fa fa-tachometer', liClass:null},
                    {label : 'Cases', state : 'admin.portfolio.case-list', iconClass : 'fa fa-sitemap', liClass:null},
                    {label : 'Users', state : 'admin.users.list', iconClass : 'fa fa-users', liClass:null}
                ],
                'client' : [
                    {label : 'Client', state : 'admin.portfolio.dashboard', iconClass : 'fa fa-tachometer', liClass:null}
                ],
                'prospector' : [
                    {label : 'Prospector', state : 'admin.portfolio.dashboard', iconClass : 'fa fa-tachometer', liClass:null}
                ]
            }
        };


        // exposes the public methods/vars
        return {
            /* vars */
            self:self,
            fromLogin : fromLogin,
            leftSideBar : leftSideBar
        };


    }

    angular
        .module('PortfolioAdmin.structure')
        .factory('StructureService', StructureService);

})();