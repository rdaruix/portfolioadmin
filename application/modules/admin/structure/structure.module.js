/*
* Structure main module
* */
(function(){
    // ecma6/jshint
    'use strict';

    // creating the module
    angular
        .module('PortfolioAdmin.structure', []);

})();