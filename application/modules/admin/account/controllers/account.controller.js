(function(){
    'use strict';

    AccountCtrl.$inject = ['$q','$state', 'AccountDataService', 'AppConfigs', 'SessionService', 'toastr'];
    function AccountCtrl($q, $state, AccountDataService, AppConfigs, SessionService, toastr){

        // to be used inside returned promises
        var vm = this;

        // the main model, this model is inside the data service
        // so when the controller is loaded again the information
        // isn't loaded again without need
        var model = AccountDataService.model;
        var uploader = null;
        /*
        * Public methods
        * */

        // starts the form, fires private
        // methods that must be fired only when
        // the edit account form is loaded
        function initEditAccForm(){
        }


        // the account edit save method
        function save(editAccForm){
            if(editAccForm.$invalid)
                return false;
            // first, execute the save action
            AccountDataService.saveEdit(vm.model.user)
                .then(function(result){
                    // updates the account information stored in cache
                    AccountDataService.getAccount(false)
                        .then(function(result){

                            vm.model.currentAccount = result.data;
                            if(result.slug === "response-ok"){
                                toastr.success('ACCOUNT_SETTINGS_FORM_SUBMIT_SUCCESS_MESSAGE', 'ACCOUNT_SETTINGS_FORM_SUBMIT_SUCCESS_MESSAGE_TITLE');
                            }
                            return result;
                        });
                    return result;
                });
        }

        /*
        * Private methods
        * */

        // for when the current account information
        // is loaded and available
        function prepareFormModel(){

            // gets the current account from API or cache
            return AccountDataService.getAccount()
                .then(function(result){

                    if(result.slug === "response-ok"){

                        vm.model.currentAccount = result.data;

                        // gets info in vars to code clean
                        var currentAcc = vm.model.currentAccount;
                        var personal_info = vm.model.user.personal_info;

                        // fills the basic account information
                        personal_info.name  = currentAcc.name;
                        personal_info.email = currentAcc.email;
                        personal_info.phone = currentAcc.phone;
                        personal_info.address.country_id = currentAcc.address.data.country_id;

                    }
                    return result;
                });
        }

        /*
        * Locale methods
        * */



        // expose public methods/vars
        angular.extend(this, {

            /* vars */
            model : model,
            uploader : uploader,

            /* methods */
            initEditAccForm : initEditAccForm,
            save : save

        });
    }

    angular
        .module('PortfolioAdmin.account')
        .controller('AccountCtrl', AccountCtrl);

})();