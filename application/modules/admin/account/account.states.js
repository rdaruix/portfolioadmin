(function(){
    // ecma6 / jshint
    'use strict';

    /*
     * Get the returned states from getStates function
     * and send it to the configureStates function that
     * do the registry of new states/routes
     * */
    function appRun(routerHelper) {
        // calls the state registration method
        routerHelper.configureStates(getStates());
    }

    /*
     * Return the list of objects containing the states
     * to register
     * */
    function getStates() {
        return [
            {
                state : 'admin.account',
                config : {
                    url : '/account',
                    template : '<ui-view></ui-view>',
                    abstract : true
                }
            },
            {
                state : 'admin.account.settings',
                config : {
                    url : '/settings',
                    templateUrl : 'application/modules/admin/account/partials/teste.html',
                    data : {
                        titlesTop : {
                            title : "ACCOUNT_SETTINGS_FORM_TITLE",
                            subTitle : "ACCOUNT_SETTINGS_FORM_SUB_TITLE"
                        }
                    }
                }
            }
        ];
    }

    // run the config code inside the app main module
    angular
        .module('PortfolioAdmin.account')
        .run(appRun);

})();