/**
 * User Account module and its dependencies
 * */
(function(){

    'use strict';

    angular.module('PortfolioAdmin.account',[
        'PortfolioAdmin.core'
    ]);

})();