/*
* User account service
* */
(function(){

    'use strict';

    AccountDataService.$inject = ['$http', '$cookies', '$q', '$rootScope','SessionService', 'AppConfigs', '_', 'DisplayDashAdminService', '$state'];
    function AccountDataService($http, $cookies, $q, $rootScope, SessionService, AppConfigs, _, DisplayDashAdminService, $state){

        // public vars
        var currentAccount = (SessionService.get('account')) ? SessionService.get('account') : [];

        // model object used in the account settings edit form
        var model = {
            user : {
                personal_info: {
                    name: "",
                    email: "",
                    phone: "",
                    password: "",
                    address: {
                        country_id: 0,
                        state_id: "",
                        city_id: "",
                        zip_code: ""
                    }
                }
            },
            countries : {},
            states : {},
            cities : {},
            currentAccount : {}
        };

        var displayDashAdminModel = DisplayDashAdminService.model;
        // exports public methods
        return {
            /* methods */

            saveEdit : saveEdit,
            getAccount : getAccount,
            getCurrentAccount : getCurrentAccount,

            /* vars */
            model : model,
            currentAccount : currentAccount
        };


        //////////////////

        /*
        * Return the cached current user object
        * note: this method RETURNS FROM CACHE
        * */
        function getCurrentAccount(){
            return SessionService.get('account');
        }

        /*
        * Gets the user information from the cookie
        * note: this method PERSIST AND RETURN CACHE
        * @param fromCache {bool} - Defines if the data must be retrieved from
        * cache or from a new API call, default false
        * */
        function getAccount(fromCache){

            var self = this,
                apiUrl = AppConfigs.apiEndpoint;

            fromCache = (typeof fromCache === "undefined") ? true : fromCache;

            // if the info must come from the cache
            if(fromCache){
                var getFromCache = SessionService.get('account');
                // if there isn't a cache
                if(!getFromCache){
                    // just get the info from API
                    return getAccountFromAPI();
                } else {
                    // if there is cache, returns a promise with
                    // the cached data in it
                    return $q.when(getFromCache);
                }
            } else{
                // if the info doesn't need to come from cache
                // do the API call
                return getAccountFromAPI();
            }

            /* do the API call */
            function getAccountFromAPI(){

                return $http.get(apiUrl+'/user/public')
                    .then(getAccountSuccess)
                    .catch(getAccountFail);

                function getAccountSuccess(response){

                    // sets the local var currentAccount to the value
                    // this is the reliable source of information for account
                    // information, the cookie jui is only used to store
                    // the auth cookie and some information from the account
                    // but wasn't made to be used to check information the current account
                    if(response.data.message !== "Token has expired" && response.status !== 500){
                        self.currentAccount = response.data;
                        SessionService.set('account', response.data);
                        SessionService.remove('userNotFound');
                    }else{
                        getAccountFail(response.data);
                    }
                    // updates the cache

                    // updates the jui cookie
                    var jui = SessionService.get('jui');
                    if(!jui){
                        getAccountFail(response.data)
                    }else{

                        SessionService.set('jui', jui);

                        // fires a update event
                        $rootScope.$broadcast('currentAccount:available', response.data);
                        return response.data;
                    }

                }

                function getAccountFail(error){
                    displayDashAdminModel.showDashboard = false;
                    $state.go('login.auth');
                    SessionService.remove('account'); // account info
                    return error.data;
                }
            }
        }

        /*
        * Send the PUT request to save the user account information
        * @param {object} data:
        *
        * {
        *   "personal_info": {
        *        "name": "",
        *        "email": "",
        *        "phone": "",
        *        "password": "",
        *       "address": {
        *            "country_id": 0,
        *            "state": "",
        *            "city": "",
        *            "zip_code": ""
        *        }
        *    }
        * }
        * */
        function saveEdit(data){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.put(apiUrl+'/account', data)
                .then(saveEditSuccess)
                .catch(saveEditFail);

            function saveEditSuccess(response){
                return response.data;
            }

            function saveEditFail(error){
                return error.data
            }
        }
    }

    angular
        .module('PortfolioAdmin.account')
        .factory('AccountDataService', AccountDataService)

})();