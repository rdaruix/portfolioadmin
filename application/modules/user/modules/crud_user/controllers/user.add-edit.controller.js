(function(){
    'use strict';


    UserAddEditCtrl.$inject = ['$scope', 'UserService', '$stateParams', '$mdDialog'];
    function UserAddEditCtrl($scope, UserService, $stateParams, $mdDialog){

        var userAddEditCtrl = this,
            model = angular.copy(UserService.model),
            userEdit = $stateParams.userId;
            userAddEditCtrl.modelRead = {
                roles: []
            };

        init();

        function init() {
            getRoles();
            if(userEdit){
                getEditUser(userEdit);
            }
        }


        function getRoles(){
            UserService.getRoles()
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        userAddEditCtrl.modelRead.roles = result.data;
                    }
                })
        }

        function getEditUser(userEdit){
            UserService.getUserById(userEdit)
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        userAddEditCtrl.model = result.data;
                        userAddEditCtrl.model.role = result.data.roles[0];
                    }
                })
        }

        function save(){
            UserService.addUser(userAddEditCtrl.model)
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        if(!userEdit){
                            swal(
                                'Sucesso!',
                                'Usuário cadastrado com sucesso',
                                'success'
                            );
                            userAddEditCtrl.model = angular.copy(UserService.model);
                        }else{
                            swal(
                                'Sucesso!',
                                'Usuário editado com sucesso',
                                'success'
                            );
                        }
                    }
                })
        }

        function changePassword(){

            $mdDialog.show({
                controller: ChangePasswordModalCtrl,
                controllerAs: "changePasswordModalCtrl",
                templateUrl: "change-password.modal.tpl.html",
                clickOutsideToClose: true,
                escapeToClose : false
            });
        }


        ChangePasswordModalCtrl.$inject = ["UserService"];
        function ChangePasswordModalCtrl(UserService){
            var passwordCtrl = this,
                modelPass = angular.copy(UserService.modelPass);

            function save(){
                passwordCtrl.modelPass.userId = $stateParams.userId;

                UserService.changePassword(passwordCtrl.modelPass)
                    .then(function(result){
                        if(result.slug === 'response-ok'){
                            swal(
                                'Sucesso!',
                                'Senha alterada com sucesso',
                                'success'
                            );
                            $mdDialog.cancel();
                            getTechnologies();
                        }
                    })
            }

            function cancel(){
                $mdDialog.cancel();
            }

            angular.extend(this, {
                passwordCtrl:passwordCtrl,
                modelPass:modelPass,

                cancel: cancel,
                save:save
            });
        }


        // exports
        angular.extend(this, {
            /* vars */
            model: model,

            save:save,
            changePassword:changePassword
        });
    }

    angular
        .module('PortfolioAdmin.user.crud')
        .controller('UserAddEditCtrl', UserAddEditCtrl)

})();