
(function () {

    'use strict';

    UserListCtrl.$inject = ['$scope', 'UserService', '$stateParams', 'AccountDataService'];
    function UserListCtrl($scope, UserService, $stateParams, AccountDataService) {

        var userCtrl = this,
            modelList = [],
            showAction = false,
            currentAccount = AccountDataService.getCurrentAccount(),
            roleAccount = null;

        init();

        function init() {

            getUserList();

            roleAccount = currentAccount.roles[0];
        }

        function getUserList(){
            UserService.getUsers()
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        userCtrl.modelList = result.data;
                    }
                });
        }

        function removeUser(userId){
            UserService.removeUser(userId)
                .then(function(result){
                    if(result.slug === 'response-ok'){
                        swal(
                            'Sucesso!',
                            'Usuário removido com sucesso',
                            'success'
                        );
                        getUserList();
                    }
                });
        }

        // exports
        angular.extend(this, {
            /* vars */
            modelList: modelList,
            roleAccount: roleAccount,
            currentAccount: currentAccount,

            removeUser: removeUser
        });

    }


    angular
        .module('PortfolioAdmin.user.crud')
        .controller('UserListCtrl', UserListCtrl)

})();