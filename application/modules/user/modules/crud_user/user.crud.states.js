/*
 * The routes for the dashboard module from the maker module
 * */
(function(){
    // ecma6 / jshint
    'use strict';

    /*
     * Get the returned states from getStates function
     * and send it to the configureStates function that
     * do the registry of new states/routes
     * */
    function appRun(routerHelper) {
        // calls the state registration method
        routerHelper.configureStates(getStates());
    }

    /*
     * Return the list of objects containing the states
     * to register
     * */
    function getStates() {
        return [
            {

                state : 'admin.users.list',
                config : {
                    url : '/users',
                    templateUrl : 'application/modules/user/modules/crud_user/partials/user.list.partial.html',
                    data : {
                        titlesTop: {
                            title : "Users"
                        }
                    }
                }
            },
            {
                state : 'admin.users.add',
                config : {
                    url : '/addUser',
                    templateUrl : 'application/modules/user/modules/crud_user/partials/user.add-edit.partial.html',
                    data : {
                        titlesTop: {
                            title : "Add User"
                        }
                    }
                }
            },
            {
                state : 'admin.users.edit',
                config : {
                    url : '/editUser/{userId:int}',
                    templateUrl : 'application/modules/user/modules/crud_user/partials/user.add-edit.partial.html',
                    data : {
                        titlesTop: {
                            title : "Edit User"
                        }
                    }
                }
            }
        ];
    }

    // run the config code inside the app main module
    angular
        .module('PortfolioAdmin.user.crud')
        .run(appRun);

})();
