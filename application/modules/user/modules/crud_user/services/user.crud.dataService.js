(function(){
    'use strict';

    UserService.$inject = ['$http', 'AppConfigs'];
    function UserService($http, AppConfigs){

        var model = {
            name:null,
            email:null,
            image:null,
            roles: []
        };

        var modelPass = {
            userId : null,
            password: null
        };

        function getUsers(){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/user/getUsers/')
                .then(getUsersSuccess)
                .catch(getUsersFail);

            function getUsersSuccess(response){
                return response.data;
            }
            function getUsersFail(error){
                return error.data;
            }
        }

        function addUser(data){
            var apiUrl = AppConfigs.apiEndpoint;


            var formData = new FormData();
            angular.forEach(data, function(value, index){
                // when the index is for the image object
                // things needs to change a bit
                if(index == 'image' && value != null){
                    formData.append('image', value);
                }else {
                    // for everything else, just appends to the formData
                    formData.append(index, value);
                }

            });

            return $http.post(apiUrl+'/user/addUser', formData, {
                headers: {'Content-Type': undefined}
            })
                .then(addUserSuccess)
                .catch(addUserFail);

            function addUserSuccess(response){
                return response.data;
            }
            function addUserFail(error){
                return error.data;
            }
        }

        function getRoles(){
            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/role/getRoles')
                .then(getRolesSuccess)
                .catch(getRolesFail);

            function getRolesSuccess(response){
                return response.data;
            }
            function getRolesFail(error){
                return error.data;
            }
        }

        function getUserById(userId){

            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/user/getUserById/'+userId)
                .then(getUserByIdSuccess)
                .catch(getUserByIdFail);

            function getUserByIdSuccess(response){
                return response.data;
            }
            function getUserByIdFail(error){
                return error.data;
            }
        }

        function removeUser(userId){

            var apiUrl = AppConfigs.apiEndpoint;

            return $http.get(apiUrl+'/user/removeUser/'+userId)
                .then(removeUserSuccess)
                .catch(removeUserFail);

            function removeUserSuccess(response){
                return response.data;
            }
            function removeUserFail(error){
                return error.data;
            }
        }

        function changePassword(data){

            var dataFormatted = {
                userId : data.userId,
                password :  data.password
            };


            var apiUrl = AppConfigs.apiEndpoint;

            return $http.post(apiUrl+'/user/changePass/', dataFormatted)
                .then(changePasswordSuccess)
                .catch(changePasswordFail);

            function changePasswordSuccess(response){
                return response.data;
            }
            function changePasswordFail(error){
                return error.data;
            }
        }

        // exports
        return {
            modelPass:modelPass,
            model: model,

            getUsers: getUsers,
            addUser: addUser,
            getRoles: getRoles,
            getUserById:getUserById,
            removeUser:removeUser,
            changePassword: changePassword
        };
    }

    angular
        .module('PortfolioAdmin.user.crud')
        .factory('UserService', UserService);

})();