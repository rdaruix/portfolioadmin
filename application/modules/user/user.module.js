(function(){
    'use strict';

    angular.module('PortfolioAdmin.user', [
        /* shared modules */
        'PortfolioAdmin.core',
        'PortfolioAdmin.user.crud'
    ]);

})();