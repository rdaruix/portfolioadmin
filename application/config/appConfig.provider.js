/**
 * Application general configs provider
 * */
(function(){

    function AppConfigs(){

        // the options object
        var options = {
            activeEnv : "production", // sets the active env
            production : {
                apiEndpoint : 'http://portfolioapi.daruixtecnologia.com.br/v1', // note: without the last slash (/)
                apiEndpointIE9 : 'http://portfolioapi.daruixtecnologia.com.br/v1'// for IE9 compatibility - same domain endpoint (no cors)
            }
        };

        // sets the active env object to return
        this.$get = [function () {
            var activeOptions = options[options['activeEnv']];
            if (activeOptions.apiEndpointIE9 && activeOptions.apiEndpointIE9.length > 0 && navigator.userAgent.match(/MSIE 9/)) {
                activeOptions.apiEndpoint = activeOptions.apiEndpointIE9;
                delete activeOptions.apiEndpointIE9;
            }
            return activeOptions;
        }];

    }

    // inject the provider in the module
    angular
        .module('PortfolioAdmin')
        .provider('AppConfigs', AppConfigs);

})();