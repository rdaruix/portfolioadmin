/*
* Directive used to place a body class based on which state the client
* is viewing, its uses the data object for each created state
* */
(function(){
    // ecma6/jshint
    'use strict';

    // the directive main function
    bodyClassDirective.$inject = ['$cookies', '$state', '$rootScope', 'SessionService'];
    function bodyClassDirective($cookies, $state, $rootScope, SessionService){

        // we only need the link function here, since we are only
        // changing the DOM
        function bodyClassDirectiveLink(scope, elem, attrs){

            var routesIgnored = [
                'login.auth',
                'login.recovery',
                'login.recoveryConfirm',
                'signup.form'
            ];
            // when the state/route start to change
            scope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState){
                    // checks if there is any configuration
                    if(event.targetScope.showDashboard === false){
                        if(routesIgnored.indexOf($state.$current.toString()) === -1){
                            // removes the authentication cookie
                            $cookies.remove('jui');
                            $cookies.remove('jcc');
                            // redirect the user to the login area
                            $state.go('login.auth')
                                .then(function(){
                                });
                        }
                    }
                    if(toState.data){

                        // if so, looks for the bodyClass object in the data object
                        var bodyClass = toState.data.bodyClass || null;

                        // checks if the previous state had a body class
                        // to remove before applying the new class
                        if(fromState.data){
                            var previousBodyClass = fromState.data.bodyClass || null;
                            if(previousBodyClass){
                                attrs.$removeClass(previousBodyClass);
                            }
                        }

                        // add the class to the body
                        if(bodyClass){
                            attrs.$addClass(bodyClass);
                        }
                    }
                }
            );
        }

        return {
            restrict : 'A', // attributes only
            link : bodyClassDirectiveLink
        }
    }

    angular
        .module('PortfolioAdmin.core')
        .directive('bodyClass', bodyClassDirective);
})();