/*
* Directive used to place a body class based on which state the client
* is viewing, its uses the data object for each created state
* */
(function(){
    // ecma6/jshint
    'use strict';

    // the directive main function
    contentClassDirective.$inject = ['$state'];
    function contentClassDirective($state){

        // we only need the link function here, since we are only
        // changing the DOM
        function contentClassDirectiveLink(scope, elem, attrs){

            // when the directive is loaded, check for state params
            if($state.$current.data){
                var contentClass = $state.$current.data.contentClass || null;
                if(contentClass){
                    attrs.$addClass(contentClass);
                }
            }

            // when the state/route start to change
            // checks if there is any configuration
            scope.$on('$stateChangeSuccess', function(e, toState, toParams, fromState){

                if(fromState.data){
                    var previousContentClass = fromState.data.contentClass || null;
                    if(previousContentClass){
                        attrs.$removeClass(previousContentClass);
                    }
                }
                if(toState.data){
                    var contentClass = toState.data.contentClass || null;
                    attrs.$addClass(contentClass);
                }

            });

        }

        return {
            restrict : 'A', // attributes only
            link : contentClassDirectiveLink
        }
    }

    angular
        .module('PortfolioAdmin.core')
        .directive('contentClass', contentClassDirective);
})();