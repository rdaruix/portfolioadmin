(function(){

    'use strict';

    function DisplayDashAdminService(){
        var model = {
            showDashboard : false
        };
        // exports public stuff
        return {
            /*variables*/
            model : model
        };

    }

    angular
        .module('PortfolioAdmin.core')
        .factory('DisplayDashAdminService', DisplayDashAdminService)

})();