(function () {
    'use strict';
    function dashAdmin(){

        dashAdminCtrl.$inject = ['SessionService', '$rootScope', 'DisplayDashAdminService', '$templateCache'];
        function dashAdminCtrl(SessionService, $rootScope, DisplayDashAdminService, $templateCache){
            var vm = this;
            var model = DisplayDashAdminService.model;

            $rootScope.$on('$stateChangeStart',
                function(){
                    var dash = SessionService.get('dash');
                    if(dash && dash.showDashboard === false){
                        $templateCache.removeAll();
                        vm.model.showDashboard = false;
                    }else{
                        if(SessionService.get('dash')){
                            vm.model.showDashboard = true;
                        }
                    }
                });

            angular.extend(this, {
                model : model
            });
        }

        // the directive config

        return {
            restrict : 'A', // attribute only
            controller : dashAdminCtrl,
            controllerAs : 'dashAdminVm'
        }
    }
    angular
        .module('PortfolioAdmin.core')
        .directive('dashAdmin', dashAdmin)
})();