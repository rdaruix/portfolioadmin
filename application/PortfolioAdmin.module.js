
// creates the main module and inject its dependencies and external modules
angular.module('PortfolioAdmin', [

        /* shared modules */
        'PortfolioAdmin.core',

        /* shared feature areas */
        'PortfolioAdmin.admin',

        /* portfolio modules*/
        'PortfolioAdmin.portfolio',
        'PortfolioAdmin.user'
    ]);