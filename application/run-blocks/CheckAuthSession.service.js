/*
* Checks if the user have a login session active
* when the app is loaded
* */
(function(){
    'use strict';

    CheckAuthSession.$inject = ['$cookies', '$state', '$rootScope', 'SessionService', 'AccountDataService', 'RecentActivitySocket'];
    function CheckAuthSession($cookies, $state, $rootScope, SessionService, AccountDataService, RecentActivitySocket){

        /*
        * Routes to be ignored by the auth session validation
        * */
        var routesIgnored = [
            'login.auth',
            'login.recovery',
            'login.recoveryConfirm',
            'signup.form'
        ];

        // exports the public methods/vars
        return {
            routesIgnored : routesIgnored,
            initialize : initialize
        };

        ////////////////////

        /*
        * Init function, registers the rootScope stateChange to the event
        * */
        function initialize(){

            AccountDataService.getAccount(false);

            // if in some point any request made towards the api
            // return an 401, the "loginRequired" event is dispatched,
            // at this point we just listen to this event and redirects
            // the user to the login page
            $rootScope.$on('loginRequired', function(){

                SessionService.remove('jui');
                if(routesIgnored.indexOf($state.$current.toString()) === -1){
                    // removes the authentication cookie
                    $cookies.remove('jui');
                    $cookies.remove('jcc');
                    // redirect the user to the login area
                    $state.go('login.auth')
                        .then(function(){
                            if(RecentActivitySocket.socket){
                                RecentActivitySocket.disconnect();
                            }
                        });
                }
            });

            // checks for the cookie existence in the user browser
            // every time he changes the current state (navigates to any page)
            $rootScope.$on('$stateChangeStart',
                function(event, toState){
                    // if the toState isn't the login page
                    if(routesIgnored.indexOf(toState.name) === -1){
                        checkCookie(event, toState);
                    }
                }
            );
        }

        /*
        * Checks if login cookie (jui) exists,
        * if true, check if its information stills valid,
        * if false, check if the users is trying to access another route than
        * the login route and redirects properly
        * */
        function checkCookie(event, toState){

            // gets the cookie
            var jui = SessionService.get('jui');
            // checks if the cookie was found
            if(!jui){
                // if false, prevent the default action to the event
                // and redirects to login page
                event.preventDefault();
                event.noUpdate = true;

                $state.go('login.auth')
                    .then(function(){
                        // after redirecting disconnects from socket
                        RecentActivitySocket.disconnect();
                        SessionService.remove('jui');
                    });


                // check if the user have roles stored in the cookie
                // if not, redirect to the profile choose page
                // only if the user isn't in this page already
                if(jui && !jui.roles.length){
                    var ignore = [
                        'profile.choose',
                        'profile.complete',
                        'signup.form'
                    ];
                    if(ignore.indexOf(toState.name) === -1){
                        event.preventDefault();
                        event.noUpdate = true;
                        $state.go('profile.choose');
                    }
                }
            }
        }
    }

    angular
        .module('PortfolioAdmin.account')
        .factory('CheckAuthSession', CheckAuthSession);

})();