/*
 * Changes the template for the toaster messages
 * so we can use the translate filter in it
 * */
(function(){
    'use strict';

    ToastrTemplate.$inject = ['$templateCache'];
    function ToastrTemplate($templateCache){

        // exports the public methods/vars
        return {
            initialize : initialize
        };

        ////////////////////

        // changes the template to add the translate filters
        // wherever the title and message goes
        function initialize(){
            $templateCache.put("directives/toast/toast.html",
                "<div class=\"{{toastClass}} {{toastType}}\" ng-click=\"tapToast()\">\n  " +
                "<div ng-switch on=\"allowHtml\">\n    " +
                "<div ng-switch-default ng-if=\"title\" class=\"{{titleClass}}\" aria-label=\"{{title|translate}}\">{{title|translate}}</div>\n    " +
                "<div ng-switch-default class=\"{{messageClass}}\" aria-label=\"{{message|translate}}\">{{message|translate}}</div>\n    " +
                "<div ng-switch-when=\"true\" ng-if=\"title\" class=\"{{titleClass}}\" ng-bind-html=\"title\"></div>\n    " +
                "<div ng-switch-when=\"true\" class=\"{{messageClass}}\" ng-bind-html=\"message\"></div>\n  " +
                "</div>\n  " +
                "<progress-bar ng-if=\"progressBar\"></progress-bar>\n</div>\n"
            );
        }

    }

    angular
        .module('PortfolioAdmin.core')
        .factory('ToastrTemplate', ToastrTemplate);

})();