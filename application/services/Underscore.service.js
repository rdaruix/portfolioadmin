/*
* Service to wrap the Underscore
* utility tool so it can be used inside controllers
* */
(function(){
    'use strict';

    underscoreService.$inject = ['$window'];
    function underscoreService($window){
        return $window._; // assumes underscore has already been loaded on the page
    }

    angular
        .module('PortfolioAdmin.core')
        .factory('_', underscoreService);

})();