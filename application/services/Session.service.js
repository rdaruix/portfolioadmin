/*
* User account service
* */
(function(){
    'use strict';

    SessionService.$inject = [];
    function SessionService(){

        // exports public methods
        return {
            set : set,
            get : get,
            remove : remove
        };

        /////////////////////////////////////

        /**
         * Save value in localstorage
         */
        function set(key, value) {
            localStorage.setItem(key, JSON.stringify(value));
        }

        /**
         * get Value from localstorage
         */
        function get(key) {
            var value = localStorage.getItem(key);
            return JSON.parse(value);
        }

        /*
        * Remove a item from localstorate
        * */
        function remove(key){
            localStorage.removeItem(key);
        }

    }

    angular
        .module('PortfolioAdmin.core')
        .factory('SessionService', SessionService);

})();