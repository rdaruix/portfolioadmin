/*
* Generic cache factory services
* */
(function(){
    'use strict';

    CacheFactory.$inject = ['$cacheFactory'];
    function CacheFactory($cacheFactory){
        return $cacheFactory('serviceCache');
    }

    angular
        .module('PortfolioAdmin.core')
        .factory('CacheFactory', CacheFactory);

})();