(function(){
    // ECMA 5
    'use strict';

    // DI for this service
    RecentActivitySocketDataService.$inject = ['AppConfigs', 'AccountDataService', 'SessionService'];
    function RecentActivitySocketDataService(AppConfigs, AccountDataService, SessionService){

        // the socket.io object container
        var socket = null;

        // exports public methods and variables
        return {
            /* methods */
            initialize : initialize,
            disconnect : disconnect,
            /* variables */
            socket : socket
        };

        // initializes the socket connection, this method is called
        // from the .run() block in the application bootstrap process
        function initialize(){

            var account = AccountDataService.currentAccount.data;

            // if the path is available
            if(AppConfigs.socket) {
                var socketConfig = AppConfigs.socket.recentActivity;
                if(socketConfig && socketConfig.endpoint){
                    // do the connection and stores the socket.io object
                    // inside the socket variable, this variable is
                    // exported to public access from the service
                    socket = io(socketConfig.endpoint, socketConfig.config);
                    socket.on('connect', function () {
                        account.authToken = SessionService.get('jui').authToken;
                        socket.emit('join', account);
                    });
                    socket.connect();
                    this.socket = socket;
                }
            }
        }

        /*
        * Disconnects from the
        * socket and clear all listeners
        * */
        function disconnect(){
            if (AppConfigs.socket && socket) {
                socket.disconnect();
                socket.removeAllListeners();
            }
        }


    }

    angular
        .module('PortfolioAdmin.core')
        .factory('RecentActivitySocket', RecentActivitySocketDataService);

})();
