/*
* App general configuration block
* */
(function(){
    'use strict';

    configure.$inject = ['$provide', '$cookiesProvider', 'toastrConfig', '$animateProvider', '$httpProvider'];
    function configure ($provide, $cookiesProvider, toastrConfig, $animateProvider, $httpProvider){

        // sets the latency configuration for the loading bar
        // by default this value is 100 (ms)
        //cfpLoadingBarProvider.latencyThreshold = 1;

        /**
         * Sets default options for cookies
         * */
        // by default all cookies dies in one hour
        /*var date = new Date();
        date.setTime(date.getTime() + (60 * 60 * 1000));*/
        $cookiesProvider.defaults.path = '/';
        //$cookiesProvider.defaults.expires = date;

        // injects the httpInterceptor provider
        // to intercept all api requests and stick
        // things in it
        $httpProvider.interceptors.push('httpInterceptor');

        // Enables filtering for ngAnimate, bc this module
        // injects animation in every angular native directive related
        // with creation/show/hide of elements (ngRepeat, ngIf, ngShow, etc...)
        // So when you wanna disable these animations somewhere, just puts the
        // class="ng-animate-disabled" in the element that is implementing the animation
        $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);

        // sets the default positioning
        // for all toast messages
        angular.extend(toastrConfig, {
            positionClass : 'toast-bottom-left'
        });

    }

    angular
        .module('PortfolioAdmin.core')
        .config(configure);
})();